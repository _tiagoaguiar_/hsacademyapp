package br.com.hsacademy.highstakesapp.app.util;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.LoginActivity;
import br.com.hsacademy.highstakesapp.http.JsonResponse;
import br.com.hsacademy.highstakesapp.lang.TaskLoader;
import br.com.hsacademy.highstakesapp.loader.UserSessionLoader;
import br.com.hsacademy.highstakesapp.service.UserSessionService;

/**
 * Created by tiago on 12/18/2016.
 */
public class UserAuthenticator extends AbstractAccountAuthenticator {

    private final Context context;

    /**
     * Creates an instance of this class.
     */
    public UserAuthenticator(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * Adds an account of the specified accountType.
     */
    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType,
                             String authTokenType, String[] requiredFeatures, Bundle options) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

        Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    /**
     * Checks that the user knows the credentials of an account.
     */
    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) {
        return null;
    }

    /**
     * Returns a Bundle that contains the Intent of the activity that can be used to edit
     * the properties.
     */
    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        throw new UnsupportedOperationException();
    }

    /**
     * Gets the authtoken for an account.
     */
    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account,
                               String authTokenType, Bundle loginOptions) throws NetworkErrorException {

        // If the caller requested an authToken type we don't support, then return an error
        if (!authTokenType.equals(context.getString(R.string.authtoken_type))) {
            Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
            return result;
        }

        // Extract the username and password from the Account Manager, and ask
        // the server for an appropriate AuthToken.
        AccountManager am = AccountManager.get(context);
        String password = am.getPassword(account);

        if (password != null) {
            String authToken = null;

            try {
                UserSessionService service = new UserSessionService(TaskLoader.Method.GET, account.name, password);
                TaskLoader<UserSessionLoader> task = new TaskLoader<>(context);

                task.execute(service);
            } catch (Exception e) {
                Log.e(Constants.TAG, e.getLocalizedMessage());
            }

            if (!TextUtils.isEmpty(authToken)) {
                Bundle result = new Bundle();
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, context.getString(R.string.authtoken_type));
                result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                return result;
            }
        }

        // If we get here, then we couldn't access the user's password - so we
        // need to re-prompt them for their credentials. We do that by creating
        // an intent to display our AuthenticatorActivity panel.
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(Constants.EXTRA_EMAIL, account.name);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

        Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    /**
     * Ask the authenticator for a localized label for the given authTokenType.
     */
    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return null;
    }

    /**
     * Checks if the account supports all the specified authenticator specific features.
     */
    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) {
        Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }

    /**
     * Update the locally stored credentials for an account.
     */
    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account,
                                    String authTokenType, Bundle loginOptions) {
        return null;
    }

}

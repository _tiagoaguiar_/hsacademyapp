package br.com.hsacademy.highstakesapp.app.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import br.com.hsacademy.highstakesapp.R;

/**
 * Created by tiago on 12/17/2016.
 */

public class ToastHelper {

    /**
     * Show a custom toast.
     */
    public static void show(Activity activity, int stringId, int duration) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View view = inflater.inflate(R.layout.simple_toast,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(activity.getText(stringId));

        Toast toast = new Toast(activity);
        toast.setDuration(duration);
        toast.setView(view);
        toast.show();
    }

    /**
     * Show a custom toast.
     */
    public static void show(Activity activity, String text, int duration) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View view = inflater.inflate(R.layout.simple_toast,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(text);

        Toast toast = new Toast(activity);
        toast.setDuration(duration);
        toast.setView(view);
        toast.show();
    }

}

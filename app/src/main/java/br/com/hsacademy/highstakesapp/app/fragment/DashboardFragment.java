package br.com.hsacademy.highstakesapp.app.fragment;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.HttpURLConnection;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.HighStakesActivity;
import br.com.hsacademy.highstakesapp.app.LoginActivity;
import br.com.hsacademy.highstakesapp.app.util.ToastHelper;
import br.com.hsacademy.highstakesapp.lang.OnRequestListener;
import br.com.hsacademy.highstakesapp.lang.TaskLoader;
import br.com.hsacademy.highstakesapp.loader.UserAccountLoader;
import br.com.hsacademy.highstakesapp.model.UserAccount;
import br.com.hsacademy.highstakesapp.service.UserSessionService;

import static br.com.hsacademy.highstakesapp.lang.TaskLoader.Method.DELETE;
import static br.com.hsacademy.highstakesapp.lang.TaskLoader.Method.GET;

/**
 * Created by tiago on 12/18/2016.
 */
public class DashboardFragment extends Fragment {

    public static final String KEY_TYPE = "keyType";

    private static final int TABS_LENGTH = 3;

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private SocialMediaFragment[] mFragments;

    /**
     * Default constructor required by Android
     */
    public DashboardFragment() {
        mFragments = new SocialMediaFragment[TABS_LENGTH];
        for (int i = 0; i < TABS_LENGTH; i++) {
            SocialMediaFragment fragment = new SocialMediaFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(KEY_TYPE, i);
            fragment.setArguments(bundle);

            mFragments[i] = fragment;
        }
    }

    /**
     * Create fragment and retain instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    /**
     * Destroy fragment and unretain instance
     */
    @Override
    public void onDestroy() {
        setRetainInstance(false);
        super.onDestroy();
    }

    /**
     * Initialize the contents of the Activity's standard options menu.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.user_connected, menu);
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menu_exit) {
            logout();
            return true;
        }
        return false;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void logout() {
        AccountManager accountManager = AccountManager.get(getActivity());
        Account account = accountManager.getAccountsByType(getString(R.string.account_type))[0];
        final String authToken = accountManager.peekAuthToken(account, getString(R.string.authtoken_type));

        accountManager.removeAccount(account, new AccountManagerCallback<Boolean>() {
            @Override
            public void run(AccountManagerFuture<Boolean> future) {
                UserSessionService service = new UserSessionService(DELETE, authToken);
                TaskLoader<Integer> task = new TaskLoader<>(DashboardFragment.this.getContext());
                task.setOnRequestListener(logoutListener);
                task.execute(service);
            }
        }, null);
    }

    public OnRequestListener<Integer> logoutListener = new OnRequestListener<Integer>() {
        @Override
        public void onRequest(Integer status) {
            switch (status.intValue()) {
                case HttpURLConnection.HTTP_OK:
                    startActivity(new Intent(DashboardFragment.this.getActivity(), LoginActivity.class));
                    DashboardFragment.this.getActivity().finish();
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    ToastHelper.show(DashboardFragment.this.getActivity(), R.string.internal_error, Toast.LENGTH_LONG);
                    break;
            }
        }
    };

    /**
     * Configures the tabs and viewpager
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_socialmedia, null);
        mTabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);

        mViewPager.setAdapter(new SocialMediaAdapter(getChildFragmentManager()));
        mTabLayout.post(new Runnable() {
            @Override
            public void run() {
                mTabLayout.setupWithViewPager(mViewPager);
            }
        });

        return view;
    }

    /**
     * Inner class that inflate {@link SocialMediaFragment}
     */
    private class SocialMediaAdapter extends FragmentPagerAdapter {

        public SocialMediaAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments[position];
        }

        @Override
        public int getCount() {
            return mFragments.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            CharSequence title = null;
            if(isAdded()) {
                switch (position){
                    case 0 : title = getResources().getText(R.string.articles); break;
                    case 1 : title = getResources().getText(R.string.videos); break;
                    case 2 : title = getResources().getText(R.string.podcasts); break;
                }
            }
            return title;
        }

    }

}
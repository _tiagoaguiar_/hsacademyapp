package br.com.hsacademy.highstakesapp.loader;

import br.com.hsacademy.highstakesapp.http.JsonWrapper;
import br.com.hsacademy.highstakesapp.model.UserAccount;

/**
 * Created by tiago on 12/17/2016.
 */

public class UserAccountLoader extends Loader<UserAccount> {

    public UserAccountLoader(JsonWrapper json, Integer status) {
        super(json, status);
    }

    @Override
    public UserAccount get() {
        return new Builder(json).build();
    }

    private class Builder implements UserAccount.Builder {

        private final JsonWrapper json;

        public Builder(JsonWrapper json) {
            this.json = json;
        }

        @Override
        public UserAccount build() {
            return new UserAccount(this);
        }

        @Override
        public String getName() {
            return json.getString("name");
        }

        @Override
        public String getLastName() {
            return json.getString("lastName");
        }

        @Override
        public String getEmail() {
            return json.getString("email");
        }

        @Override
        public String getPassword() {
            return null;
        }

    }

}
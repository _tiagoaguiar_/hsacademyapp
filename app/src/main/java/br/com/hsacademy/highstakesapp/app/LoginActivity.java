package br.com.hsacademy.highstakesapp.app;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.util.Constants;
import br.com.hsacademy.highstakesapp.app.util.StringUtils;
import br.com.hsacademy.highstakesapp.app.util.ToastHelper;
import br.com.hsacademy.highstakesapp.lang.OnRequestListener;
import br.com.hsacademy.highstakesapp.lang.TaskLoader;
import br.com.hsacademy.highstakesapp.loader.UserSessionLoader;
import br.com.hsacademy.highstakesapp.service.UserSessionService;

import static br.com.hsacademy.highstakesapp.lang.TaskLoader.Method.GET;
import static br.com.hsacademy.highstakesapp.lang.TaskLoader.Method.POST;

public class LoginActivity extends AppCompatActivity implements YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_DIALOG_REQUEST = 1;

    private Animation mButtonFlickerAnimation;

    private EditText mEditEmail;
    private EditText mEditPassword;
    private TextView mTxtCreate;
    private Button mBtnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        mButtonFlickerAnimation = AnimationUtils.loadAnimation(this, R.anim.flicker);

        findViewById(R.id.rootLayout).requestFocus();
        mEditEmail = (EditText) findViewById(R.id.editEmail);
        mEditPassword = (EditText) findViewById(R.id.editPassword);

        mTxtCreate = (TextView) findViewById(R.id.txtCreate);
        mBtnLogin = (Button) findViewById(R.id.btnLogin);

        mTxtCreate.setOnClickListener(txtCreateListener);
        mBtnLogin.setOnClickListener(btnLoginListener);

        FragmentManager fm = getSupportFragmentManager();
        YouTubePlayerSupportFragment player = (YouTubePlayerSupportFragment) fm.findFragmentById(R.id.youtube);
        player.initialize(getResources().getString(R.string.google_developer_key), this);
    }

    /**
     * This is only necessary to dispose a login screen after signup (now the user
     * has a token)
     */
    @Override
    protected void onResume() {
        super.onResume();
        AccountManager accountManager = AccountManager.get(this);
        Account accounts[] = accountManager.getAccountsByType(getString(R.string.account_type));
        if (accounts != null && accounts.length > 0) {
            finish();
        }
    }

    public View.OnClickListener txtCreateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            v.startAnimation(mButtonFlickerAnimation);
            mButtonFlickerAnimation.setAnimationListener(new StartActivityAfterAnimation(intent));
        }
    };

    public View.OnClickListener btnLoginListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (StringUtils.checkIsNotEmpty(LoginActivity.this, mEditEmail, mEditPassword)) {
                // request
                UserSessionService service = new UserSessionService(POST,
                        mEditEmail.getText().toString(), mEditPassword.getText().toString());
                TaskLoader<UserSessionLoader> task = new TaskLoader<>(LoginActivity.this);
                task.setOnRequestListener(loginResponseListener);
                task.execute(service);
            }
        }
    };

    public OnRequestListener<UserSessionLoader> loginResponseListener = new OnRequestListener<UserSessionLoader>() {
        @Override
        public void onRequest(UserSessionLoader loader) {
            switch (loader.getStatus().intValue()) {
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    ToastHelper.show(LoginActivity.this, R.string.user_or_password_incorrect, Toast.LENGTH_LONG);
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    ToastHelper.show(LoginActivity.this, R.string.internal_error, Toast.LENGTH_LONG);
                    break;
                case HttpURLConnection.HTTP_OK:
                    String authtoken = loader.get().getToken();
                    Log.i(Constants.TAG, authtoken);

                    // User autheticated
                    final String authority = getString(R.string.authority);
                    Account account = new Account(mEditEmail.getText().toString(), getString(R.string.account_type));
                    AccountManager accountManager = AccountManager.get(LoginActivity.this);

                    // Add the account explicity
                    accountManager.addAccountExplicitly(account, mEditPassword.getText().toString(), null);
                    accountManager.setAuthToken(account, getString(R.string.authtoken_type), authtoken);

                    // Set periodic synchronization: every day
                    ContentResolver.setIsSyncable(account, authority, 1);
                    ContentResolver.setSyncAutomatically(account, authority, true);
                    ContentResolver.addPeriodicSync(account, authority, new Bundle(), 86400L);

                    // Request synchronization right now
                    Bundle extras = new Bundle();
                    extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    ContentResolver.requestSync(account, authority, extras);

                    // Finish the login process
                    loader.get().getUserAccount().getName();
                    Intent intent = new Intent(LoginActivity.this, HighStakesActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                    break;
            }
        }
    };

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo(getResources().getString(R.string.youtube_video_id));
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
//            String errorMessage = String.format(getString(R.string.error_youtube_player), errorReason.toString());
//            ToastHelper.show(this, errorMessage, Toast.LENGTH_LONG);
        }
    }

    private class StartActivityAfterAnimation implements Animation.AnimationListener {
        private Intent mIntent;

        StartActivityAfterAnimation(Intent intent) {
            mIntent = intent;
        }

        public void onAnimationEnd(Animation animation) {
            startActivity(mIntent);
            try {
                // invoke animation for this activity
                Method method = Activity.class.getMethod("overridePendingTransition",
                        new Class[]{Integer.TYPE, Integer.TYPE});
                method.invoke(LoginActivity.this, R.anim.activity_fade_in, R.anim.activity_fade_out);

            } catch (NoSuchMethodException e) {
                Log.i(Constants.TAG, "Failed to launch main activity", e);
            } catch (InvocationTargetException e) {
                Log.i(Constants.TAG, "Failed to invoke method in Splash Screen", e);
            } catch (IllegalAccessException e) {
                Log.i(Constants.TAG, "Failed to invoke method in Splash Screen", e);
            }
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

}

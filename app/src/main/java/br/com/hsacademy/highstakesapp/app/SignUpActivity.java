package br.com.hsacademy.highstakesapp.app;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.HttpURLConnection;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.util.Constants;
import br.com.hsacademy.highstakesapp.app.util.StringUtils;
import br.com.hsacademy.highstakesapp.app.util.ToastHelper;
import br.com.hsacademy.highstakesapp.http.JsonResponse;
import br.com.hsacademy.highstakesapp.lang.OnRequestListener;
import br.com.hsacademy.highstakesapp.lang.TaskLoader;
import br.com.hsacademy.highstakesapp.loader.UserSessionLoader;
import br.com.hsacademy.highstakesapp.service.UserAccountService;

import static br.com.hsacademy.highstakesapp.lang.TaskLoader.Method.POST;

/**
 * Created by tiago on 12/17/2016.
 */
public class SignUpActivity extends AppCompatActivity {

    private EditText mEditName;
    private EditText mEditLastName;
    private EditText mEditEmail;
    private EditText mEditPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mEditName = (EditText) findViewById(R.id.editName);
        mEditLastName = (EditText) findViewById(R.id.editLastName);
        mEditEmail = (EditText) findViewById(R.id.editEmail);
        mEditPassword = (EditText) findViewById(R.id.editPassword);

        Button btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(btnSendListener);
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }
        return false;
    }

    public View.OnClickListener btnSendListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // validate
            if (StringUtils.checkIsNotEmpty(SignUpActivity.this, mEditName, mEditLastName, mEditEmail, mEditPassword)) {
                // request
                UserAccountService service = new UserAccountService(POST, mEditName.getText().toString(),
                        mEditLastName.getText().toString(),
                        mEditEmail.getText().toString(),
                        mEditPassword.getText().toString());

                TaskLoader<UserSessionLoader> task = new TaskLoader<>(SignUpActivity.this);
                task.setOnRequestListener(signUpResponseListener);
                task.execute(service);
            }
        }
    };

    public OnRequestListener<UserSessionLoader> signUpResponseListener = new OnRequestListener<UserSessionLoader>() {
        @Override
        public void onRequest(UserSessionLoader response) {
            switch (response.getStatus().intValue()) {
                case HttpURLConnection.HTTP_OK:
                    ToastHelper.show(SignUpActivity.this, R.string.user_account_created_successful, Toast.LENGTH_LONG);
                    String authtoken = response.get().getToken();
                    Log.i(Constants.TAG, authtoken);

                    // User autheticated
                    final String authority = getString(R.string.authority);
                    Account account = new Account(mEditEmail.getText().toString(), getString(R.string.account_type));
                    AccountManager accountManager = AccountManager.get(SignUpActivity.this);

                    // Add the account explicity
                    accountManager.addAccountExplicitly(account, mEditPassword.getText().toString(), null);
                    accountManager.setAuthToken(account, getString(R.string.authtoken_type), authtoken);

                    // Set periodic synchronization: every day
                    ContentResolver.setIsSyncable(account, authority, 1);
                    ContentResolver.setSyncAutomatically(account, authority, true);
                    ContentResolver.addPeriodicSync(account, authority, new Bundle(), 86400L);

                    // Request synchronization right now
                    Bundle extras = new Bundle();
                    extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    ContentResolver.requestSync(account, authority, extras);

                    // Finish the login process
                    Intent intent = new Intent(SignUpActivity.this, HighStakesActivity.class);
                    startActivity(intent);
                    SignUpActivity.this.finish();
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    ToastHelper.show(SignUpActivity.this, R.string.user_account_duplicated, Toast.LENGTH_LONG);
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    ToastHelper.show(SignUpActivity.this, R.string.internal_error, Toast.LENGTH_LONG);
                    break;
            }
        }
    };

}
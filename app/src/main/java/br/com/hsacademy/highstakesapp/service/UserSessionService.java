package br.com.hsacademy.highstakesapp.service;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.util.Constants;
import br.com.hsacademy.highstakesapp.app.util.StringUtils;
import br.com.hsacademy.highstakesapp.http.HighStakesWS;
import br.com.hsacademy.highstakesapp.http.JsonResponse;
import br.com.hsacademy.highstakesapp.lang.TaskLoader.Method;
import br.com.hsacademy.highstakesapp.loader.UserAccountLoader;
import br.com.hsacademy.highstakesapp.loader.UserSessionLoader;

/**
 * Created by tiago on 12/17/2016.
 */

public class UserSessionService<T> implements Service<T> {

    private String token;
    private Method method;
    private String email;
    private String password;

    public UserSessionService(Method method, String email, String password) {
        this.method = method;
        this.email = email;
        this.password = password;
    }

    public UserSessionService(Method method) {
        this(method, null);
    }

    public UserSessionService(Method method, String token) {
        this.method = method;
        this.token = token;
    }

    @Override
    public T execute(Context context) {
        T element = null;
        switch (method) {
            case POST: element = (T) authenticate(email, password); break;
            case GET: element = (T) getUserAccount(context); break;
            case DELETE: element = (T) invalidate(token); break;
        }
        return element;
    }

    private T authenticate(String email, String password) {
        JsonResponse response = HighStakesWS.of(method, "/rest/userSession")
                .param("email", email)
                .param("password", StringUtils.encodePassword(password))
                .request();
        return (T) new UserSessionLoader(response.getJson(), response.getStatus());
    }

    private T invalidate(String token) {
        JsonResponse response = HighStakesWS.of(method, "/rest/userSession")
                .param("token", token)
                .request();
        return (T) response.getStatus();
    }

    private T getUserAccount(Context context) {
        AccountManager accountManager = AccountManager.get(context);

        Account accounts[] = accountManager.getAccountsByType(context.getString(R.string.account_type));
        if (accounts == null || accounts.length < 1) {
            return (T) new UserAccountLoader(null, HttpURLConnection.HTTP_UNAUTHORIZED);
        }

        try {
            String token = accountManager.blockingGetAuthToken(accounts[0],
                    context.getString(R.string.authtoken_type), false);

            JsonResponse response = HighStakesWS.of(method, "/rest/userSession")
                    .param("token", token)
                    .request();
            return (T) new UserAccountLoader(response.getJson(), response.getStatus());
        } catch (Exception e) {
            Log.e(Constants.TAG, "error in block token", e);
            return (T) new UserAccountLoader(null, HttpURLConnection.HTTP_UNAUTHORIZED);
        }

    }

}
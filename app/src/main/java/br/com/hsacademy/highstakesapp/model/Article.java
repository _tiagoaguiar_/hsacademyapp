package br.com.hsacademy.highstakesapp.model;

import br.com.hsacademy.highstakesapp.lang.Builder;

/**
 * Created by tiago on 12/18/2016.
 */
public class Article {

    private final String title;
    private final String description;

    public Article(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}

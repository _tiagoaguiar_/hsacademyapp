package br.com.hsacademy.highstakesapp.http;

/**
 * Created by tiago on 12/17/2016.
 */
public class WebServiceException extends Exception {

    private final Integer status;

    public WebServiceException(String detailMessage, int status) {
        super(detailMessage);
        this.status = status;
    }

    public Integer status() {
        return status;
    }

}
package br.com.hsacademy.highstakesapp.model;

import br.com.hsacademy.highstakesapp.lang.Builder;

/**
 * Created by tiago on 12/17/2016.
 */

public class UserSession {

    private final UserAccount userAccount;
    private final String token;

    public interface Builder extends br.com.hsacademy.highstakesapp.lang.Builder<UserSession> {
        
        UserAccount getUserAccount();
        
        String getToken();
        
    }
    
    public UserSession(Builder builder) {
        userAccount = builder.getUserAccount();
        token = builder.getToken();
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public String getToken() {
        return token;
    }
}

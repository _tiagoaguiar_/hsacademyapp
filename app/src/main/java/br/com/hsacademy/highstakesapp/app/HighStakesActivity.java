package br.com.hsacademy.highstakesapp.app;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.net.HttpURLConnection;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.fragment.DashboardFragment;
import br.com.hsacademy.highstakesapp.app.fragment.ProfileFragment;
import br.com.hsacademy.highstakesapp.app.util.Constants;
import br.com.hsacademy.highstakesapp.app.util.ToastHelper;
import br.com.hsacademy.highstakesapp.lang.OnRequestListener;
import br.com.hsacademy.highstakesapp.lang.TaskLoader;
import br.com.hsacademy.highstakesapp.loader.UserAccountLoader;
import br.com.hsacademy.highstakesapp.loader.UserSessionLoader;
import br.com.hsacademy.highstakesapp.model.UserAccount;
import br.com.hsacademy.highstakesapp.service.UserSessionService;

import static br.com.hsacademy.highstakesapp.lang.TaskLoader.Method.GET;

public class HighStakesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;

    private FrameLayout mContentFrameLayout;
    private boolean isDrawerLocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.highstakes);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_drawer);
        mContentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);

        createNavigation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Used only for DrawerToggle
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserSessionService service = new UserSessionService(GET);
        TaskLoader<UserAccountLoader> task = new TaskLoader<>(this);
        task.setOnRequestListener(sessionListener);
        task.execute(service);
    }

    public OnRequestListener<UserAccountLoader> sessionListener = new OnRequestListener<UserAccountLoader>() {
        @Override
        public void onRequest(UserAccountLoader loader) {
            switch (loader.getStatus().intValue()) {
                case HttpURLConnection.HTTP_OK:
                    // design header view
                    View headerView =  mNavigationView.getHeaderView(0);
                    RelativeLayout profileBg = (RelativeLayout) headerView.findViewById(R.id.profileBg);
                    Resources res = getResources();
                    int color = ContextCompat.getColor(HighStakesActivity.this, R.color.colorAccent);
                    profileBg.getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);

                    UserAccount user = loader.get();
                    ((TextView)headerView.findViewById(R.id.txtHeaderName))
                            .setText(getResources().getString(R.string.welcome, user.getName()));

                    ((TextView)headerView.findViewById(R.id.txtHeaderEmail)).setText(user.getEmail());
                    onNavigationItemSelected(mNavigationView.getMenu().findItem(R.id.menu_dashboard));
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    ToastHelper.show(HighStakesActivity.this, R.string.user_or_password_incorrect, Toast.LENGTH_LONG);
                    Intent intent = new Intent(HighStakesActivity.this, LoginActivity.class);
                    startActivity(intent);
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    ToastHelper.show(HighStakesActivity.this, R.string.internal_error, Toast.LENGTH_LONG);
                    break;
            }
        }
    };

    /**
     * Used only for DrawerToggle
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Create and inflate the menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * Listener for menu action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Create and config the navigation view, actionbar (toolbar)
     * and drop down spinner
     */
    private void createNavigation() {
        if (((ViewGroup.MarginLayoutParams) mContentFrameLayout.getLayoutParams()).leftMargin ==
                (int) getResources().getDimension(R.dimen.drawer_size)) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, mNavigationView);
            mDrawerLayout.setScrimColor(Color.TRANSPARENT);
            mDrawerLayout.setFocusableInTouchMode(false);
            isDrawerLocked = true;
        }

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        if (!isDrawerLocked) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
            }
        };

        // this block remove arrow <- from action bar (used only tablets)
        if (isDrawerLocked) {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationIcon(R.mipmap.ic_launcher);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {}
            });
            mDrawerLayout.openDrawer(mNavigationView);
        }

        mNavigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.menu_dashboard:
                fragment = new DashboardFragment();
                break;
            case R.id.menu_profile:
                fragment = new ProfileFragment();
                break;
            case R.id.menu_settings:
            case R.id.menu_rate:
            case R.id.menu_about:
                ToastHelper.show(HighStakesActivity.this, R.string.come_soon, Toast.LENGTH_LONG);
                break;
        }

        mNavigationView.getMenu().clear();
        mNavigationView.inflateMenu(R.menu.menu_drawer);
        mNavigationView.getMenu().findItem(menuItem.getItemId()).setChecked(true);

        if (!isDrawerLocked) {
            mDrawerLayout.closeDrawer(mNavigationView);
        } else {
            supportInvalidateOptionsMenu();
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, "tabFragmentTag").commit();
        }

        if (!isDrawerLocked)
            mDrawerLayout.closeDrawer(mNavigationView);

        return true;
    }


    /**
     * When we click at frame_layout body execute this method to avoid an error at DrawerLayout
     * E/MessageQueue-JNI(31443): at android.support.v4.widget.DrawerLayout.isContentView(DrawerLayout.java:853)
     * E/MessageQueue-JNI(31443): Exception in MessageQueue callback: handleReceiveCallback
     */
    public void fragmentClick(View v) {
        return;
    }

}
package br.com.hsacademy.highstakesapp.app;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.util.Constants;

/**
 * Created by tiago on 12/18/2016.
 */
public class SplashScreenActivity extends AppCompatActivity {

    private static final int UI_ANIMATION_DELAY = 300;

    private final Handler mHideHandler = new Handler();

    private View mContentView;
    private View mBackgroundView;
    private Animation mFadeOutAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        mContentView = findViewById(R.id.linear_full_screen_content);
        mBackgroundView = findViewById(R.id.background_view);

        // config animations
        mFadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.activity_fade_out);

        // animate the red dot
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);

        // highlight the image
        findViewById(R.id.txtDot).startAnimation(anim);
        Animation animation = new TranslateAnimation(0, 0, -500, 0);
        animation.setDuration(1000);
        animation.setFillAfter(true);
        findViewById(R.id.imgLogo).startAnimation(animation);
        findViewById(R.id.imgLogo).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
        delayedShow(5000);
    }

    /**
     * Choose between main features or login.
     * It depends of token.
     */
    private void show() {
        Intent intent;
        AccountManager accountManager = AccountManager.get(this);
        Account accounts[] = accountManager.getAccountsByType(getString(R.string.account_type));
        if (accounts == null || accounts.length < 1) {
            intent = new Intent(getBaseContext(), LoginActivity.class);
        } else {
            intent = new Intent(getBaseContext(), HighStakesActivity.class);
        }

        mFadeOutAnimation.setAnimationListener(new StartActivityAnimation(intent));
        mBackgroundView.startAnimation(mFadeOutAnimation);
    }

    /**
     * Hide UI Systems
     */
    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Runnable to show UI Systems.
     */
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };

    /**
     * Runnable to hide UI Systems.
     */
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    /**
     * Runnable to hide UI Systems.
     */
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    /**
     * Runnable to show UI Systems.
     */
    private final Runnable mShowRunnable = new Runnable() {
        @Override
        public void run() {
            show();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    /**
     * Schedules a call to show() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedShow(int delayMillis) {
        mHideHandler.removeCallbacks(mShowRunnable);
        mHideHandler.postDelayed(mShowRunnable, delayMillis);
    }

    /**
     * Inner class that handler activity fade_out
     */
    private class StartActivityAnimation implements Animation.AnimationListener {

        private final Intent mIntent;

        StartActivityAnimation(Intent intent) {
            mIntent = intent;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            startActivity(mIntent);
            try {
                // invoke animation for this activity
                Method method = Activity.class.getMethod(
                        "overridePendingTransition", new Class[]{Integer.TYPE, Integer.TYPE});
                method.invoke(SplashScreenActivity.this, R.anim.activity_fade_in, R.anim.activity_fade_out);

                // Show the system bar
                mContentView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

                // Schedule a runnable to display UI elements after a delay
                mHideHandler.removeCallbacks(mHidePart2Runnable);
                mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
            } catch (NoSuchMethodException e) {
                Log.i(Constants.TAG, "Failed to launch main activity", e);
            } catch (InvocationTargetException e) {
                Log.i(Constants.TAG, "Failed to invoke method in Splash Screen", e);
            } catch (IllegalAccessException e) {
                Log.i(Constants.TAG, "Failed to invoke method in Splash Screen", e);
            } finally {
                finish();
            }
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

    }

}

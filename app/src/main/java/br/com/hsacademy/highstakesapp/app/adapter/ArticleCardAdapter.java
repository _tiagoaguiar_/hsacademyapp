package br.com.hsacademy.highstakesapp.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.model.Article;

/**
 * Created by tiago on 12/18/2016.
 */
public class ArticleCardAdapter extends RecyclerView.Adapter<ArticleCardAdapter.ArticleViewHolder>{

    private final ArrayList<Article> articles;

    public ArticleCardAdapter(ArrayList<Article> articles) {
        this.articles = articles;
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ArticleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_card, parent, false));
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        holder.title.setText(articles.get(position).getTitle());
        holder.description.setText(articles.get(position).getDescription());
    }

    public static class ArticleViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView description;

        public ArticleViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txtTitle);
            description = (TextView) itemView.findViewById(R.id.txtDescription);
        }

    }

}
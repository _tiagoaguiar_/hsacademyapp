package br.com.hsacademy.highstakesapp.app.util;

/**
 * Created by tiago on 12/17/2016.
 */

public final class Constants {

    public static final String TAG = "highstakesapp";
    public static final String EXTRA_EMAIL = "br.com.hsacademy.extra.email";

}
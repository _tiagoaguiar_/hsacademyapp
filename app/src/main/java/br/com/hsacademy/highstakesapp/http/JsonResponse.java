package br.com.hsacademy.highstakesapp.http;

/**
 * Created by tiago on 12/17/2016.
 */

public class JsonResponse {

    private final Integer status;
    private final JsonWrapper json;

    public JsonResponse(Integer status, JsonWrapper json) {
        this.status = status;
        this.json = json;
    }

    public Integer getStatus() {
        return status;
    }

    public JsonWrapper getJson() {
        return json;
    }

}

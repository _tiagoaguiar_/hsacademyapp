package br.com.hsacademy.highstakesapp.service;

import android.content.Context;

import br.com.hsacademy.highstakesapp.app.util.StringUtils;
import br.com.hsacademy.highstakesapp.http.HighStakesWS;
import br.com.hsacademy.highstakesapp.http.JsonResponse;
import br.com.hsacademy.highstakesapp.lang.TaskLoader.Method;
import br.com.hsacademy.highstakesapp.loader.UserSessionLoader;

/**
 * Created by tiago on 12/17/2016.
 */

public class UserAccountService<T> implements Service<T> {

    private final Method method;
    private final String name;
    private final String lastname;
    private final String email;
    private final String password;

    public UserAccountService(Method method, String name, String lastname, String email, String password) {
        this.method = method;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
    }

    @Override
    public T execute(Context context) {
        T element = null;
        switch (method) {
            case POST: element = (T) insertUser(name, lastname, email, password); break;
        }
        return element;
    }

    private T insertUser(String name, String lastname, String email, String password) {
        JsonResponse response = HighStakesWS.of(method, "/rest/userAccount")
                .param("name", name)
                .param("lastname", lastname)
                .param("email", email)
                .param("password", StringUtils.encodePassword(password))
                .request();

        return (T) new UserSessionLoader(response.getJson(), response.getStatus());
    }

}
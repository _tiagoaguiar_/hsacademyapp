package br.com.hsacademy.highstakesapp.model;

import org.joda.time.LocalDateTime;

import java.io.Serializable;

import br.com.hsacademy.highstakesapp.lang.Builder;

/**
 * Created by tiago on 12/17/2016.
 */

public class UserAccount implements Serializable {

    private final String name;
    private final String lastName;
    private final String email;
    private final String password;
    private LocalDateTime createdDate;

    public interface Builder extends br.com.hsacademy.highstakesapp.lang.Builder<UserAccount> {

        String getName();

        String getLastName();

        String getEmail();

        String getPassword();

    }

    public UserAccount(Builder builder) {
        name = builder.getName();
        lastName = builder.getLastName();
        email = builder.getEmail();
        password = builder.getPassword();
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }
}
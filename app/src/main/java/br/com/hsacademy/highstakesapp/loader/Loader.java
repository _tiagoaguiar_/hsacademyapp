package br.com.hsacademy.highstakesapp.loader;

import br.com.hsacademy.highstakesapp.http.JsonWrapper;

/**
 * Created by tiago on 12/17/2016.
 */

abstract class Loader<T> {

    protected final Integer status;
    protected final JsonWrapper json;

    public Loader(JsonWrapper json, Integer status) {
        this.json = json;
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public abstract T get();

}
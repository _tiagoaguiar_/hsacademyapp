package br.com.hsacademy.highstakesapp.app.util;

import android.app.Activity;
import android.util.Base64;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import br.com.hsacademy.highstakesapp.R;

/**
 * Created by tiago on 12/17/2016.
 */

public final class StringUtils {

    public static boolean checkIsNotEmpty(Activity activity, EditText... editTexts) {
        for (int i = 0; i < editTexts.length; i++) {
            EditText editText = editTexts[i];
            if (editText.getText().length() < 1) {
                String text = activity.getString(R.string.validate_not_empty, editText.getTag());
                ToastHelper.show(activity, text, Toast.LENGTH_LONG);
                return false;
            }
        }
        return true;
    }

    public static String encodePassword(String password) {
        byte[] data = new byte[0];
        try {
            data = password.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decodePassword(String password) {
        byte[] data = Base64.decode(password, Base64.DEFAULT);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}

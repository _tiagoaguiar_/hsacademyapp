package br.com.hsacademy.highstakesapp.http;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

/**
 * Created by tiago on 12/17/2016.
 */

public class JsonWrapper {

    private final JSONObject json;

    public JsonWrapper(JSONObject json) {
        this.json = json;
    }

    public JSONArray getJSONArray(String key) {
        try {
            return json.has(key) ? json.getJSONArray(key) : null;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public int length() {
        return getJSONArray("data").length();
    }

    public JsonWrapper getJSONObject(int index) {
        try {
            JSONObject _json = json.getJSONArray("data").getJSONObject(index);
            return new JsonWrapper(_json);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonWrapper getJSONObject(String key) {
        try {
            return json.has(key) ? new JsonWrapper(json.getJSONObject(key)) : null;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public String getString(String key) {
        try {
            return json.has(key) ? json.getString(key) : null;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer getInt(String key) {
        try {
            return json.has(key) ? json.getInt(key) : null;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public Long getLong(String key) {
        try {
            return json.has(key) ? json.getLong(key) : null;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public LocalDateTime getLocalDateTime(String key) {
        try {
            if (json.has(key)) {
                String value = json.getString(key);
                return LocalDateTime.parse(value, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ"));
            } else {
                return null;
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean hasString(String key) {
        return json.has(key);
    }

    public Double getDouble(String key) {
        try {
            return json.has(key) ? json.getDouble(key) : null;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

}

package br.com.hsacademy.highstakesapp.lang;

/**
 * Created by tiago on 12/17/2016.
 */

public interface OnRequestListener<T> {

    void onRequest(T element);

}

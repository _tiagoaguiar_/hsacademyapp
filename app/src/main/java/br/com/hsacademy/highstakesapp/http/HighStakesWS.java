package br.com.hsacademy.highstakesapp.http;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import br.com.hsacademy.highstakesapp.lang.TaskLoader;

import static br.com.hsacademy.highstakesapp.app.util.Constants.TAG;

/**
 * Created by tiago on 12/17/2016.
 */

public final class HighStakesWS {

    private static final String BASE_URL = "http://ec2-34-193-105-44.compute-1.amazonaws.com";
    private static HighStakesWS ws = null;

    private TaskLoader.Method method;
    private String url;
    private String params;

    private HighStakesWS(){}

    static HighStakesWS getInstance() {
        if (ws == null) {
            return new HighStakesWS();
        }
        return ws;
    }

    public static HighStakesWS of(TaskLoader.Method method, String url) {
        ws = getInstance();
        ws.method = method;
        ws.url = url;
        ws.params = ws.method == TaskLoader.Method.POST ? "" : null;
        return ws;
    }

    public HighStakesWS param(String param, String value) {
        HighStakesWS ws = getInstance();
        if (!url.contains("?")) url += "?";
            try {
                if (method == TaskLoader.Method.POST) {
                        params += param + "=" + URLEncoder.encode(value, "UTF-8") + "&";
                } else {
                    url += param + "=" + URLEncoder.encode(value, "UTF-8") + "&";
                }
            } catch (UnsupportedEncodingException e) {
                Log.w(TAG, "failed when try to encoding parameter", e);
            }
        return ws;
    }

    public JsonResponse request() {
        String _url = url.substring(0, url.length() - 1);
        StringBuilder response = new StringBuilder();
        URL urlService;
        JsonResponse jsonResponse = null;
        try {
            urlService = new URL(BASE_URL + _url);
            HttpURLConnection conn = (HttpURLConnection) urlService.openConnection();

            conn.setRequestMethod(method.name());
            conn.setDoOutput(method == TaskLoader.Method.POST ? true : false);
            conn.setReadTimeout(200000);
            conn.setConnectTimeout(10000);
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            conn.connect();

            if (params != null) {
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(params);
                wr.flush();
                wr.close();
            }

            jsonResponse = new JsonResponse(conn.getResponseCode(), null);

            if (conn.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;

                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                rd.close();
                String res = response.toString();

                if (res != null && !res.isEmpty()) {
                    JSONObject json = new JSONObject(res);
                    jsonResponse = new JsonResponse(conn.getResponseCode(), new JsonWrapper(json));
                }
            }

        } catch (MalformedURLException e) {
            Log.w(TAG, "failed to request json [malformed url]", e);
            jsonResponse = new JsonResponse(HttpURLConnection.HTTP_INTERNAL_ERROR, null);
        } catch (IOException e) {
            Log.w(TAG, "generic IO Exception when try request json", e);
            jsonResponse = new JsonResponse(HttpURLConnection.HTTP_INTERNAL_ERROR, null);
        } catch (Exception e) {
            Log.w(TAG, "failed when try build json", e);
            jsonResponse = new JsonResponse(HttpURLConnection.HTTP_INTERNAL_ERROR, null);
        }
        return jsonResponse;
    }

}
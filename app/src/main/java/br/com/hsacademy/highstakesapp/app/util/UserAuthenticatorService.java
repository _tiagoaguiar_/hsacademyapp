package br.com.hsacademy.highstakesapp.app.util;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by tiago on 12/18/2016.
 */

public class UserAuthenticatorService extends Service {

    private UserAuthenticator authenticator;

    /**
     * Called by the system when the service is first created.
     */
    @Override
    public void onCreate() {
        authenticator = new UserAuthenticator(this);
    }

    /**
     * Return the communication channel to the service.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }

}

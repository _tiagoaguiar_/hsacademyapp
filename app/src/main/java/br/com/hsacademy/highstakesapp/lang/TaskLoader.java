package br.com.hsacademy.highstakesapp.lang;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.service.Service;

/**
 * Created by tiago on 12/17/2016.
 */

public class TaskLoader<T> extends AsyncTask<Service<T>, Void, T> {

    public enum Method {
        GET,
        POST,
        PUT,
        DELETE;
    }

    private final Context mContext;
    private final ProgressDialog mDialog;
    private OnRequestListener mListener;

    public TaskLoader(Context context) {
        mContext = context;
        mDialog = new ProgressDialog(context);
        mDialog.setMessage(context.getString(R.string.wait_please));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setIndeterminate(true);
        mDialog.setOnCancelListener(cancelListener);
    }

    public void setOnRequestListener(OnRequestListener listener) {
        mListener = listener;
    }

    @Override
    protected T doInBackground(Service<T>... services) {
        return services[0].execute(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog.show();
    }

    @Override
    protected void onPostExecute(T element) {
        mDialog.dismiss();
        if (mListener != null) mListener.onRequest(element);
    }

    private DialogInterface.OnCancelListener cancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            TaskLoader.this.cancel(true);
        }
    };

}
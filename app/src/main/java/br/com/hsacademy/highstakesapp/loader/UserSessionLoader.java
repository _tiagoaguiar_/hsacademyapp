package br.com.hsacademy.highstakesapp.loader;

import br.com.hsacademy.highstakesapp.http.JsonWrapper;
import br.com.hsacademy.highstakesapp.model.UserAccount;
import br.com.hsacademy.highstakesapp.model.UserSession;

/**
 * Created by tiago on 12/17/2016.
 */

public class UserSessionLoader extends Loader<UserSession> {

    public UserSessionLoader(JsonWrapper json, Integer status) {
        super(json, status);
    }

    @Override
    public UserSession get() {
        return new Builder(json).build();
    }

    private class Builder implements UserSession.Builder {

        private final JsonWrapper json;

        public Builder(JsonWrapper json) {
            this.json = json;
        }

        @Override
        public UserSession build() {
            return new UserSession(this);
        }

        @Override
        public UserAccount getUserAccount() {
            return new UserAccountLoader(json.getJSONObject("userAccount"), status).get();
        }

        @Override
        public String getToken() {
            return json.getString("token");
        }

    }

}
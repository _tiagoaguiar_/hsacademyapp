package br.com.hsacademy.highstakesapp.app.fragment;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.hsacademy.highstakesapp.R;
import br.com.hsacademy.highstakesapp.app.adapter.ArticleCardAdapter;
import br.com.hsacademy.highstakesapp.app.util.Constants;
import br.com.hsacademy.highstakesapp.app.util.ToastHelper;
import br.com.hsacademy.highstakesapp.lang.OnRequestListener;
import br.com.hsacademy.highstakesapp.model.Article;

/**
 * Created by tiago on 12/18/2016.
 */
public class SocialMediaFragment extends Fragment {

    enum Type {
        ARTICLE,
        VIDEO,
        PODCAST;

    }

    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView.Adapter<? extends RecyclerView.ViewHolder> mAdapter;

    private ArrayList<Article> articles;
    private Type type;

    public SocialMediaFragment() {
    }

    /**
     * Create fragment
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int ordinal = getArguments().getInt(DashboardFragment.KEY_TYPE, 0);
        type = Type.values()[ordinal];
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: 12/18/2016 IMPLEMENT LOADER MANAGER
    }

    /**
     * Configures the {@link RecyclerView}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.socialmedia_fragment, container, false);

        RecyclerView recycler = (RecyclerView) view.findViewById(R.id.tab_recycler);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(view.getContext()));

        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        // TODO: 12/18/2016 REFRESH LISTENER FROM SERVER
        mRefreshLayout.setOnRefreshListener(refreshListener);
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        // TODO: CHANGE MOCK DATA
        articles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            articles.add(new Article("Title " + i, "Description " + i));
        }

        switch (type) {
            case ARTICLE:
                mAdapter = new ArticleCardAdapter(articles);
                break;
        }
        recycler.setAdapter(mAdapter);

        return view;
    }

    public SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            Log.d(Constants.TAG, "refresh..");
            ToastHelper.show(getActivity(), R.string.updating, Toast.LENGTH_LONG);

            Article article = articles.get(articles.size() - 1);
            articles.clear();
            articles.add(article);

            mAdapter.notifyDataSetChanged();
            mRefreshLayout.setRefreshing(false);
        }
    };

}

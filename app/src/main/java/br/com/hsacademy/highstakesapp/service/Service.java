package br.com.hsacademy.highstakesapp.service;

import android.content.Context;

import br.com.hsacademy.highstakesapp.http.WebServiceException;

/**
 * Created by tiago on 12/17/2016.
 */

public interface Service<T> {

    T execute(Context context);

}
